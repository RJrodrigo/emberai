﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIStateMachine;
using UnityEngine.AI;

public class AI : MonoBehaviour
{
    public bool switchState = false;
    public float turnSpeed = 10f;

    private float turnSpeedMultiplier;
    private float speed = 0f;
    public float walkSpeed = 0.2f;
    public float runSpeed = 0.75f;
    private float direction = 0f;
    private float groundCheckDistance = 0.25f;
    private bool isGrounded;
    public Animator anim;
    public Vector3 targetDirection;
    private Vector2 input;
    private float velocity;
    public float lastSlash = 100f;

    //public bool turning = false;
    public NavMeshAgent navMeshAgent;
    public Vector3 targetPoint;
    public GameObject destination;
    public GameObject previousDestination;

    public StateMachine<AI> stateMachine { get; set; }

    //public UnityEngine.AI.NavMeshAgent navMeshAgent;

    private void Start()
    {
        anim = GetComponent<Animator>();
        stateMachine = new StateMachine<AI>(this);
        stateMachine.ChangeState(PatrolState.Instance);
        navMeshAgent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
        if (navMeshAgent == null)
            Debug.Log("No NavMeshAgent attached");
    }

    private void FixedUpdate()
    {
        // use a point of view obj to detect the player(since they smell instead of seeing use a sphere)
        // if player is in the sphere then switchState = true
        // else remain in the same state

        LookForPlayer();
        stateMachine.Update();


        //Debug.DrawRay(transform.position, targetPoint - transform.position, Color.green);
        navMeshAgent.SetDestination(targetPoint);

        input.x = 0;
        
        CheckGroundStatus();
        anim.SetBool("OnGround", isGrounded);

        // Update target direction relative to the camera view (or not if the Keep Direction option is checked)
        //UpdateTargetDirection();

    }

    private void LookForPlayer()
    {
        float radius = 10f;
        Vector3 npcPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(npcPos, radius);
        bool found = false;
        foreach (Collider hit in colliders)
        {
            // check if is player
            // One way is having the player's position and comparing both
            // Another way is giving the player model a script that identifies it as a player
            if (hit.gameObject.GetComponent<Player>() != null)
            {
                targetPoint = hit.transform.position;
                float dist = Vector3.Magnitude(targetPoint - transform.position);
                if (dist < 1.5f)
                    stateMachine.ChangeState(AttackState.Instance);
                else if (!(stateMachine.currentState is ChaseState))
                    stateMachine.ChangeState(ChaseState.Instance);
                found = true;
            }
        }
        if (!found && !(stateMachine.currentState is PatrolState))
            stateMachine.ChangeState(PatrolState.Instance);
    }

    public virtual void UpdateTargetDirection()
    {

        // The step size is equal to speed times frame time.
        float singleStep = speed * Time.deltaTime;

        // Rotate the forward vector towards the target direction by one step
        Vector3 newDirection = Vector3.RotateTowards(transform.forward, targetDirection, 0.05f, 0.0f);
        //turning = !(newDirection == transform.forward);

        // Calculate a rotation a step closer to the target and applies rotation to this object
        transform.rotation = Quaternion.LookRotation(newDirection);
    }

    void CheckGroundStatus()
    {
        // 0.1f is a small offset to start the ray from inside the character
        // it is also good to note that the transform position in the sample assets is at the base of the character
        if (Physics.Raycast(transform.position + (Vector3.up * 0.1f), Vector3.down, groundCheckDistance))
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }

    //Trigger Kirin Audio Effects
    //These are called by Events in each animation file. The names need to match the ones set there.
    //If the name is changed, the Event in the animation file needs to be changed as well.
    void IdlePant() => KirinAudioPlayer.instance.PlayAudioIdlePant();
    void IdleCaution() => KirinAudioPlayer.instance.PlayAudioIdleCaution();
    void IdleClean() => KirinAudioPlayer.instance.PlayAudioIdleClean();
    void WalkForward() => KirinAudioPlayer.instance.PlayAudioWalkForwardFootSteps();
    void RunForward() => KirinAudioPlayer.instance.PlayAudioRunForwardFootSteps();
    void Jump() => KirinAudioPlayer.instance.PlayAudioJump();
    void JumpLand() => KirinAudioPlayer.instance.PlayAudioJumpLand();
    void Slash() => KirinAudioPlayer.instance.PlayAudioSlash();
}
