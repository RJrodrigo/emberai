﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIStateMachine;

public class AttackState : State<AI>
{

    private static AttackState _instance;

    public AttackState()
    {
        if (_instance != null)
            return;
        _instance = this;
    }

    public static AttackState Instance
    {
        get
        {
            if (_instance == null)
                new AttackState();
            return _instance;
        }
    }

    public override void EnterState(AI _o)
    {
        Debug.Log("Entering Attack State.");
    }

    public override void ExitState(AI _o)
    {
        Debug.Log("Exiting Attack State.");
        _o.lastSlash = 5f;
    }

    public override void Update(AI _o)
    {
        if (_o.switchState)
        {
            _o.stateMachine.ChangeState(PatrolState.Instance);
        } else
        {

            float speed = _o.runSpeed;
            _o.anim.SetFloat("Speed", speed);
            if (_o.lastSlash > 1.5f)
            {
                _o.anim.SetTrigger("AttackSlash");
                _o.lastSlash = 0f;
            } else
            {
                _o.lastSlash += Time.deltaTime;
            }
        }
    }
}
