﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AIStateMachine;
using System;

public class PatrolState : State<AI>
{

    private static PatrolState _instance;

    public PatrolState()
    {
        if (_instance != null)
            return;
        _instance = this;
    }

    public static PatrolState Instance
    {
        get
        {
            if (_instance == null)
                new PatrolState();
            return _instance;
        }
    }

    public override void EnterState(AI _o)
    {
        Debug.Log("Entering Patrol State.");
        //pickTarget(_o);
        findDestination(_o);
    }

    public override void ExitState(AI _o)
    {
        Debug.Log("Exiting Patrol State.");
    }

    public override void Update(AI _o)
    {
        if(_o.switchState)
        {
            _o.stateMachine.ChangeState(ChaseState.Instance);
            _o.switchState = !_o.switchState;
        } else 
        {
            /*float speed = _o.walkSpeed;
            _o.anim.SetFloat("Speed", speed);

            //Debug.Log(_o.navMeshAgent.remainingDistance);
            if (1.5f > _o.navMeshAgent.remainingDistance)
                pickTarget(_o);*/

            if (2f > _o.navMeshAgent.remainingDistance)
                refreshDestination(_o);

            if (_o.destination != null)
            {
                float speed = _o.walkSpeed;
                _o.anim.SetFloat("Speed", speed);
            }
        }        
    }

    private void refreshDestination(AI _o)
    {
        Waypoint waypoint = _o.destination.GetComponent<Waypoint>();
        GameObject dest = waypoint.getRandNeighbour();
        GameObject temp = _o.destination;

        if (dest == null && _o.previousDestination != null)
            _o.destination = _o.previousDestination;
        else
            _o.destination = dest;

        _o.previousDestination = temp;
        _o.targetPoint = _o.destination.transform.position;

    }

    private bool findDestination(AI _o)
    {
        var objectsWithTag = GameObject.FindGameObjectsWithTag("Waypoint");
        GameObject closest = null;
        foreach (GameObject obj in objectsWithTag)
        {
            if (obj.GetComponent<Waypoint>() != null)
            {
                if (closest == null)
                    closest = obj;
                else if(Vector3.Distance(_o.transform.position, obj.transform.position) < Vector3.Distance(_o.transform.position, closest.transform.position))
                    closest = obj;
            }

        }
        if (closest != null)
        {
            _o.destination = closest;
            _o.targetPoint = _o.destination.transform.position;
        }
        return closest == null;
    }

    private void pickTarget(AI _o)
    {
        if (_o.navMeshAgent.destination != null)
            Debug.Log("Picking distance at: " + _o.navMeshAgent.remainingDistance);
        else
            Debug.Log("Picking distance.");

        

        RaycastHit hit;
        Ray ray = new Ray();
        ray.origin = _o.transform.position + _o.transform.up * .25f;
        float maxDist = 0f;
        Vector3 target;

        Debug.DrawRay(ray.origin, _o.transform.right*10f, Color.green, 10);
        Debug.DrawRay(ray.origin, -_o.transform.right * 10f, Color.green, 10);
        Debug.DrawRay(ray.origin, _o.transform.forward * 10f, Color.green, 10);
        Debug.DrawRay(ray.origin, -_o.transform.forward * 10f, Color.green, 10);

        // Pick the one further away
        if (Physics.Raycast(ray.origin, _o.transform.right, out hit, 1000f))
        {
            Debug.DrawRay(ray.origin, _o.transform.right, Color.green);
            if (hit.distance > maxDist)
            {
                Debug.Log("Right got it at: " + hit.distance);
                maxDist = hit.distance;
                target = hit.point - _o.transform.right * 2f;
                _o.targetPoint = new Vector3(target.x, _o.transform.position.y, target.z);
            }
        }
        if (Physics.Raycast(ray.origin, -_o.transform.right, out hit, 1000f))
        {
            Debug.DrawRay(ray.origin, -_o.transform.right, Color.green);
            if (hit.distance > maxDist)
            {
                Debug.Log("Left got it at: " + hit.distance);
                maxDist = hit.distance;
                target = hit.point + _o.transform.right * 2f;
                _o.targetPoint = new Vector3(target.x, _o.transform.position.y, target.z);
            }
        }
        if (Physics.Raycast(ray.origin, _o.transform.forward, out hit, 1000f))
        {
            Debug.DrawRay(ray.origin, _o.transform.forward, Color.green);
            if (hit.distance > maxDist)
            {
                Debug.Log("Front got it at: " + hit.distance);
                maxDist = hit.distance;
                target = hit.point - _o.transform.forward * 2f;
                _o.targetPoint = new Vector3(target.x, _o.transform.position.y, target.z);
            }
        }
        if (Physics.Raycast(ray.origin, -_o.transform.forward, out hit, 1000f))
        {
            if (hit.distance > maxDist)
            {
                Debug.Log("Back got it at: " + hit.distance);
                maxDist = hit.distance;
                target = hit.point + _o.transform.forward * 2f;
                _o.targetPoint = new Vector3(target.x, _o.transform.position.y, target.z);
            }
        }
    }
}
