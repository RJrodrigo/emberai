﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    private GameObject[] neighbours;
    private int count = 0;
    public float maxDistance = 20f;

    // Start is called before the first frame update
    void Start()
    {
        neighbours = new GameObject[10];
        findNeighbours();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void findNeighbours()
    {
        var objectsWithTag = GameObject.FindGameObjectsWithTag("Waypoint");
        foreach (GameObject obj in objectsWithTag)
        {
            if (obj.GetComponent<Waypoint>() != null)
                if (Vector3.Distance(transform.position, obj.transform.position) <= maxDistance)
                    neighbours[count++] = obj;
            if (count >= 10)
                break;

        }
    }

    public GameObject getRandNeighbour()
    {
        System.Random random = new System.Random();
        return count > 0 ? neighbours[random.Next(0, count)] : null;
    }
}
